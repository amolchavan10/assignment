/// <reference types="cypress" />

describe('Sign Up Tests', () => {
    before(() => {
        cy.visit('')
        cy.addNewUser()

    })



    it.only('Feed Screen for new  User who has not added Any feed.', () => {
        const user = Cypress.env('USER_1')
        cy.contains('Login').click()
        cy.get('#id_username').type(user.username)
        cy.get('#id_password').type(user.password)
        cy.get('.bs-component > .btn').click()
        cy.contains('My Feeds').click()
        cy.contains('Nothing to see here. Move on!').should('exist')
        cy.contains('New Feed').should('exist')
        cy.contains('Logout').should('exist')
        cy.contains('All Feeds').should('exist')
        cy.contains('Bookmarked').should('exist')
    })



    it.only('Logged out user sees already added a feed', () => {
        //Already added on item in the feed
        const feed = Cypress.env('FEED').RSS1
        cy.contains('Logout').click()
        cy.contains('All Feeds').click()
        cy.getFeedTitleFromTable().should('have.text', feed.title)
    })

    it('Duplicate - Feed : If Feed is already exist', () => {
        const feed = Cypress.env('FEED').RSS1
        cy.contains('New Feed').click()
        cy.get('#id_feed_url').type(feed.url)
        cy.contains('Submit').click()
        cy.contains('Feed with this Feed URL already exists.').should('exist')
        cy.contains('All Feeds').click()
        cy.getFeedTitleFromTable().should('have.text', feed.title)
        cy.contains('My Feeds').click()
        cy.getFeedTitleFromTable().should('have.text', feed.title)
    })


    it.only('Feed bookmarks', () => {
        cy.contains('Logout').click()
        cy.addNewUser()
        cy.contains('Bookmarked').click()
        cy.getFeedTitleFromTable().should('not.exist')
        cy.contains('Nothing to see here. Move on!').should('exist')

        cy.contains('All Feeds').click()
        cy.getFeedTitleFromTable().first().click()
        getBookmarkIcon().click()
        getActiveBookmarkIcon().should('exist')

        cy.contains('Bookmarked').click()
        cy.getFeedTitleFromTable().first().click()
        getActiveBookmarkIcon().should('exist')
    })




    it('Error message for Invalid URL .', () => {
        const user = Cypress.env('USER_1')
        cy.contains('Login').click()
        cy.get('#id_username').type(user.username)
        cy.get('#id_password').type(user.password)
        cy.get('.bs-component > .btn').click()
        cy.contains('New Feed').click()
        cy.contains('Submit').click()
        cy.contains('This field is required.').should('exist')
        cy.contains('Logout').click()
    })


    it('Error message for URL which is not RSS .', () => {
        const user = Cypress.env('USER_1')
        cy.contains('Login').click()
        cy.get('#id_username').type(user.username)
        cy.get('#id_password').type(user.password)
        cy.get('.bs-component > .btn').click()
        cy.contains('New Feed').click()
        cy.get('#id_feed_url').type('https://www.google.com')
        cy.contains('Submit').click()
        cy.contains('Enter a valid URL.').should('exist')
        // Bug exist, no Validation if URL is valid RSS or NOT
        // cy.contains('Logout').click()

    })


    function getBookmarkIcon() {
        return cy.get('.glyphicon-heart-empty')
    }

    function getActiveBookmarkIcon() {
        return cy.get('.glyphicon-heart')
    }


})