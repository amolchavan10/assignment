/// <reference types="cypress" />

describe('Login Tests', () => {
  beforeEach(() => {
    cy.visit('')
    cy.contains('Login').click()
  })



  it('Empty Inputs for Login shows proper error message', () => {
    cy.get('.bs-component > .btn').click()
    cy.get('#error_1_id_username > strong').should('exist')
    cy.get('#error_1_id_password > strong').should('exist')
  })

  it('Invalid Username', () => {
    const user = Cypress.env('USER_1')
    cy.get('#id_username').type(user.username)
    cy.get('#id_password').type("PASSWORD_2")
    cy.get('.bs-component > .btn').click()
    cy.get('.alert > ul > li').contains('Please enter a correct username and password. Note that both fields may be case-sensitive.').should('exist')
  })

  it('Invalid Password', () => {
    const user = Cypress.env('USER_1')
    cy.get('#id_username').type("USER_2")
    cy.get('#id_password').type(user.password)
    cy.get('.bs-component > .btn').click()
    cy.get('.alert > ul > li').contains('Please enter a correct username and password. Note that both fields may be case-sensitive.').should('exist')
  })


  it('Valid Login', () => {
    const user = Cypress.env('USER_1')
    cy.get('#id_username').type(user.username)
    cy.get('#id_password').type(user.password)
    cy.get('.bs-component > .btn').click()
    cy.contains('Logout').should('exist')
  })



})
