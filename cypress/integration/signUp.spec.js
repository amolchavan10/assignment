/// <reference types="cypress" />

describe('Sign Up Tests', () => {
    beforeEach(() => {
        cy.visit('')
        cy.contains('Sign Up').click()
    })



    it('Duplicate Username is not allowed for Sign up', () => {
        let username = cy.faker.internet.userName()
        let password = cy.faker.internet.password()
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password)
        cy.get('#submit-id-submit').click()
        cy.contains('Great success! Enjoy :').should('exist')
        cy.contains('Logout').click()
        cy.contains('Sign Up').click()
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password)
        cy.get('#submit-id-submit').click()
        cy.contains('A user with that username already exists.').should('exist')
    })


    it('On Password and Password confirmation mismatch, sign up is not allowed', () => {
        let username = cy.faker.internet.userName()
        let password = cy.faker.internet.password()
        let password2 = cy.faker.internet.password() + "21"
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password2)
        cy.get('#submit-id-submit').click()
        cy.contains("The two password fields didn't match.").should('exist')
    })



    it('Password with less than 8 characters is not allowed', () => {
        let username = cy.faker.internet.userName()
        let password = "QWERTY2"  //Less than 8 characters
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password)
        cy.get('#submit-id-submit').click()
        cy.contains("This password is too short. It must contain at least 8 characters.").should('exist')
    })

    it('Invalid Username', () => {
        let username = cy.faker.internet.userName() + "$"
        let password = cy.faker.internet.password()
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password)
        cy.get('#submit-id-submit').click()
        cy.contains("Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.").should('exist')
    })



    it('Blank fields', () => {
        cy.get('#submit-id-submit').click()
        cy.contains('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.').should('exist')
        cy.contains('This field is required.').should('exist')
        cy.contains("Enter the same password as before, for verification.").should('exist')
    })



    it('User is able to Sign Up using Valid Details', () => {
        let username = cy.faker.internet.userName()
        let password = cy.faker.internet.password()
        cy.get('#id_username').type(username)
        cy.get('#id_password1').type(password)
        cy.get('#id_password2').type(password)
        cy.get('#submit-id-submit').click()
        cy.contains('Great success! Enjoy :').should('exist')
    })



})