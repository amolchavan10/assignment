Cypress.Commands.add('addNewUser', () => {
    cy.contains('Sign Up').click()
    let username = cy.faker.internet.userName()
    let password = cy.faker.internet.password()
    cy.get('#id_username').type(username)
    cy.get('#id_password1').type(password)
    cy.get('#id_password2').type(password)
    cy.get('#submit-id-submit').click()
    cy.contains('Great success! Enjoy :').should('exist')
    // cy.contains('Logout').click()
})

Cypress.Commands.add('getFeedRows', () => {
    return cy.get('.table-responsive tbody tr')
})



Cypress.Commands.add('getFeedTitleFromTable', () => {
    return cy.get('.table-responsive tr td a')
})