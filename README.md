# Assignment

Assignment for Send-Cloud


To Run the Created Cypress Tests : Application need to be running on provided URL.

**To Run the application in docker windows, pleae follow the steps:**
1. Install docker 
2. Checkout the code provided by team.
3. Update the docker-compose file for DB section and make change for Volume attribute from 
```
volumes:
    - ./tmp/pgdata:/var/lib/postgresql/data:rw
```      
to 
``` 
volumes:
    - /var/lib/postgresql/data` 
```
This is becuase of the issue mentioned here - https://forums.docker.com/t/data-directory-var-lib-postgresql-data-pgdata-has-wrong-ownership/17963/6

If one fails to follow this, below error will get displayed.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e437c4c1-5eb1-41e8-af23-6ba5e7bc6bb0/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e437c4c1-5eb1-41e8-af23-6ba5e7bc6bb0/Untitled.png)

`django.db.utils.OperationalError: could not translate host name "db" to address: Temporary failure in name resolution`

4. We can start running application using `docker-compose up` command
5. Application will listen on the port 8000 and one can access it in browser using URL - http://127.0.0.1:8000/


**To Run the Cypress code, follow below steps:**
1. Install NodeJS
2. Install Cypress
3. Go To the folder cypress committed here.
4. Install required packages using `npm install`
5. Once package are installed, test can run locally in UI mode using `npx cypress open`  or `node_modules/.bin/cypress open`
6. To run the test, in the headless mode `npx cypress run` can be used.


**Able to cover following functionality in Cypress:**
 - SingIn, 
 - Login & 
 - Feeds feature.
 
 
Annotation feature is still pending. 
Also like to try the execution of the autotests tests using GitLab CI, **Currently I tried locally using Jenkins and it works fine**. 


Other artifcats about testing stratgegy and Results are available on the below link
https://www.notion.so/Deliverables-5cae95105f21468ab8fc03b8d76918dc.



